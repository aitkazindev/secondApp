//
//  ViewController.swift
//  secondApp
//
//  Created by Ibrahim on 13.04.2021.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var ImageView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelSurname: UILabel!
    var name = ""
    var surname = ""
    var image = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelName.text = name
        labelSurname.text = surname
        ImageView.image = UIImage(named: image)
        
    }


}

